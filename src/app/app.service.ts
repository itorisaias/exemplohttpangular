import { Injectable, Inject } from '@angular/core';
import { API_URL } from './app.config';
import { Http, RequestOptions, Headers, Response } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AppService {

  constructor(
    private http: Http,
    @Inject(API_URL) private apiURL
  ) { }

  getAll(entidade: string) {
    return this.http.get(this.getUrl(entidade), this.headerJwt())
      .map(this.extractData)
      .catch(this.handleError);
  }

  get(entidade: string, id) {
    return this.http.post(this.getUrl(entidade, id), this.headerJwt())
      .map(this.extractData)
      .catch(this.handleError);
  }

  save(entidade: string, obj) {
    return this.http.post(this.getUrl(entidade), JSON.stringify(obj), this.headerJwt(true))
      .map(this.extractData)
      .catch(this.handleError);
  }

  update(entidade: string, obj) {
    return this.http.post(this.getUrl(entidade, obj.id), JSON.stringify(obj), this.headerJwt(true))
      .map(this.extractData)
      .catch(this.handleError);
  }

  delete(entidade: string, id) {
    return this.http.post(this.getUrl(entidade, id), this.headerJwt())
      .map(this.extractData)
      .catch(this.handleError);
  }

  private getUrl(entidade: string, id?: any) {
    return id ? `${this.apiURL}/${entidade}/${id}` : `${this.apiURL}/${entidade}`
  }

  private headerJwt(hasJson?: boolean) {
    let headers = new Headers({ 'Authorization': 'Bearer ' + 'YOUR TOKEN' });
    if (hasJson) headers.append('Content-Type', 'application/json');
    return new RequestOptions({ headers: headers });
  }

  private extractData(res: Response) {
    let body = res.json();
    return body.data || {};
  }

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Promise.reject(errMsg);
  }
}

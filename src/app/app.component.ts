import { AppService } from './app.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app works!';
  objects: Array<any>;
  entidade: string;
  isWork: boolean;

  constructor(
    private service: AppService
  ) { }

  ngOnInit() {
    this.entidade = 'YOUR ENTIDADE';
    this.isWork = true;

    this.service.getAll(this.entidade).subscribe(
      data => {
        this.objects = data;
        this.isWork = false;
      }, error => {
        this.isWork = false;
        alert(error);
      });
  }

  save(obj){
    let result,
        index = this.objects.length;
    this.isWork = true;

    obj.id ? result = this.service.update(this.entidade, obj) : result = this.service.save(this.entidade, obj);

    result.subscribe(
      data => {
        this.objects.splice(index, 0, data);
        this.isWork = false;
        alert(data + 'Save');
      }, error => {
        this.isWork = false;
        alert(error);
      });
  }

  delete(obj){
    if (confirm("Deseja excluir o registro " + obj.nome + "?")) {
      var index = this.objects.indexOf(obj);
      let id = this.objects[index]['id'];
      this.objects[index]['id'] = 'loader';

      this.service.delete(this.entidade, id)
        .subscribe(
        data => {
          !data? this.ngOnInit() : true; 
          this.objects.splice(index, 1);
          alert('Registro '+ obj.name +' excluir com sucesso');
        }, err => {
          alert('O registro '+ obj.name +' não pode ser excluido')
          this.objects[index]['id'] = id;
        });
    }
  }

  get(id){
    this.service.get(this.entidade, id).subscribe(
      data => {
        alert('registro ' + data);
      },error => {
        alert('não foi possivel encontrar o registro');
      });
  }
}
import { ExemploHttpPage } from './app.po';

describe('exemplo-http App', () => {
  let page: ExemploHttpPage;

  beforeEach(() => {
    page = new ExemploHttpPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
